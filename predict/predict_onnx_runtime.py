import numpy as np
import onnxruntime as ort
# Load the ONNX model
from tqdm import tqdm

def ort_session_test(ort_session):

    outputs = ort_session.run(
        [ner_name],
        ort_session_inputs,
    )
    # print(outputs)
if __name__ == '__main__':
    ort_session = ort.InferenceSession("./models/small/uer_py_ner_part0.8195248547762591.onnx")
    label_name = ort_session.get_outputs()[0].name
    ner_name = ort_session.get_outputs()[1].name
    ort_session_inputs = {}
    for input in ort_session.get_inputs():
        ort_session_inputs[str(input.name)] = np.random.randint(2, size=(7, 128))
    ort_session_test(ort_session)

    for i in tqdm(range(1000)):
        ort_session_test(ort_session)
