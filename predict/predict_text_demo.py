import argparse
import json
import os
from multiprocessing import Pool

import numpy as np
import onnxruntime as ort  # 仅仅支撑CPU推理


def convert_by_vocab(vocab, items):
    """Converts a sequence of [tokens|ids] using the vocab."""
    output = []
    for item in items:
        output.append(vocab[item] if item in vocab else vocab.get(UNK_TOKEN))
    return output


def count_lines(file_path):
    lines_num = 0
    with open(file_path, 'rb') as f:
        while True:
            data = f.read(2 ** 20)
            if not data:
                break
            lines_num += data.count(b'\n')
    return lines_num


class Vocab(object):
    """
    """

    def __init__(self):
        self.w2i = {}
        self.i2w = []
        self.w2c = {}
        self.reserved_vocab_path = \
            os.path.abspath(os.path.join(os.path.dirname(__file__), "../../models/reserved_vocab.txt"))

    def load(self, vocab_path, is_quiet=False):
        with open(vocab_path, mode="r", encoding="utf-8") as reader:
            for index, line in enumerate(reader):
                w = line.strip("\n").split()[0] if line.strip() else line.strip("\n")
                self.w2i[w] = index
                self.i2w.append(w)
        if not is_quiet:
            print("Vocabulary size: ", len(self))

    def save(self, save_path):
        print("Vocabulary size: ", len(self))
        with open(save_path, mode="w", encoding="utf-8") as f:
            for w in self.i2w:
                f.write(w + "\n")
        print("Vocabulary saving done.")

    def get(self, w):
        return self.w2i[w]

    def __len__(self):
        return len(self.i2w)

    def worker(self, corpus_path, tokenizer, start, end):
        """
        Worker that creates vocabulary from corpus[start:end].
        """
        w2i, i2w, w2c = {}, [], {}
        pos = 0
        with open(corpus_path, mode="r", encoding="utf-8") as f:
            while pos < start:
                f.readline()
                pos += 1
            while True:
                line = f.readline()
                pos += 1

                tokens = tokenizer.tokenize(line, use_vocab=False)
                for t in tokens:
                    if t not in w2i:
                        w2i[t], w2c[t] = len(i2w), 1
                        i2w.append(t)
                    else:
                        w2c[t] += 1
                if pos >= end - 1:
                    return (w2i, i2w, w2c)

    def union(self, vocab_list):
        """ Union vocab in all workers. """
        w2i, i2w, w2c = {}, [], {}
        index = 0
        for v_p in vocab_list:
            w2i_p, i2w_p, w2c_p = v_p.get()
            for w in i2w_p:
                if w not in w2i:
                    w2i[w], w2c[w] = len(i2w), w2c_p[w]
                    i2w.append(w)
                else:
                    w2c[w] += w2c_p[w]
        return (w2i, i2w, w2c)

    def build(self, corpus_path, tokenizer, workers_num=1, min_count=1):
        """ Build vocabulary from the given corpus. """
        print("Start %d workers for building vocabulary..." % workers_num)
        lines_num = count_lines(corpus_path)
        pool = Pool(workers_num)
        vocab_list = []
        for i in range(workers_num):
            start = i * lines_num // workers_num
            end = (i + 1) * lines_num // workers_num
            vocab_list.append((pool.apply_async(func=self.worker, args=[corpus_path, tokenizer, start, end])))
        pool.close()
        pool.join()

        # Union vocab in all workers.
        w2i, i2w, w2c = self.union(vocab_list)
        # Sort w2c according to word count.
        sorted_w2c = sorted(w2c.items(), key=lambda item: item[1], reverse=True)

        # Add special symbols and remove low frequency words.
        with open(self.reserved_vocab_path, mode="r", encoding="utf-8") as reader:
            self.i2w = [line.strip().split()[0] for line in reader]

        for i, w in enumerate(self.i2w):
            self.w2i[w] = i
            self.w2c[w] = -1

        for w, c in sorted_w2c:
            if c < min_count:
                break
            if w not in self.w2i:
                self.w2i[w], self.w2c[w] = len(self.i2w), c
                self.i2w.append(w)


class Tokenizer(object):

    def __init__(self, args, is_src=True):
        self.vocab = None
        self.sp_model = None
        if is_src == True:
            spm_model_path = args.spm_model_path
            vocab_path = args.vocab_path
        else:
            spm_model_path = args.tgt_spm_model_path
            vocab_path = args.tgt_vocab_path

        if spm_model_path:
            try:
                import sentencepiece as spm
            except ImportError:
                raise ImportError(
                    "You need to install SentencePiece to use XLNetTokenizer: https://github.com/google/sentencepiece"
                    "pip install sentencepiece")
            self.sp_model = spm.SentencePieceProcessor()
            self.sp_model.Load(spm_model_path)
            self.vocab = {self.sp_model.IdToPiece(i): i for i
                          in range(self.sp_model.GetPieceSize())}
        else:
            self.vocab = Vocab()
            self.vocab.load(vocab_path, is_quiet=True)
            self.vocab = self.vocab.w2i
        self.inv_vocab = {v: k for k, v in self.vocab.items()}

    def tokenize(self, text):
        raise NotImplementedError

    def convert_tokens_to_ids(self, tokens):
        if self.sp_model:
            return [self.sp_model.PieceToId(
                printable_text(token)) for token in tokens]
        else:
            return convert_by_vocab(self.vocab, tokens)

    def convert_ids_to_tokens(self, ids):
        if self.sp_model:
            return [self.sp_model.IdToPiece(id_) for id_ in ids]
        else:
            return convert_by_vocab(self.inv_vocab, ids)


with open("./models/special_tokens_map.json", mode="r", encoding="utf-8") as f:
    special_tokens_map = json.load(f)

UNK_TOKEN = special_tokens_map["unk_token"]
CLS_TOKEN = special_tokens_map["cls_token"]
SEP_TOKEN = special_tokens_map["sep_token"]
MASK_TOKEN = special_tokens_map["mask_token"]
PAD_TOKEN = special_tokens_map["pad_token"]


class SpaceTokenizer(Tokenizer):

    def __init__(self, args, is_src=True):
        super().__init__(args, is_src)

    def tokenize(self, text, use_vocab=True):
        if use_vocab:
            return [token if token in self.vocab else UNK_TOKEN for token in text.strip().split(" ")]
        else:
            return [token for token in text.strip().split(" ")]


def ort_session_test(args, ort_session, ner_name=None, ort_session_inputs=None, medical_id_label=None):
    outputs = ort_session.run(
        [ner_name],
        ort_session_inputs,
    )
    print(outputs)
    index = 0
    label_out = []
    label_out_put = []
    for output in outputs:
        print(output.tolist())
        print(len(output.tolist()))
        for output_one in output.tolist():
            label_out.append(medical_id_label[output_one])
            index += 1
            if index == args.seq_length:
                index = 0
                label_out_put.append(label_out)
                label_out = []
    return label_out_put


def text_predict(args, line=None):
    medical_ner_label_id = json.load(open(args.medical_ner_label_id))
    id_label = {}
    for medical_ner_label, medical_ner_label_id in medical_ner_label_id.items():
        id_label[medical_ner_label_id] = medical_ner_label
    src = args.tokenizer.convert_tokens_to_ids(args.tokenizer.tokenize(" ".join(line)))
    seg = [1] * len(src)
    if len(src) > args.seq_length:
        src = src[: args.seq_length]
        seg = seg[: args.seq_length]
    PAD_ID = args.tokenizer.convert_tokens_to_ids([PAD_TOKEN])[0]

    while len(src) < args.seq_length:
        src.append(PAD_ID)
        seg.append(0)
    print(src)
    print(seg)
    ort_session = ort.InferenceSession("./models/small/uer_py_ner_part0.8195248547762591.onnx")
    ner_name = ort_session.get_outputs()[1].name
    ort_session_inputs = {}
    print(ort_session.get_inputs())
    for input in ort_session.get_inputs():
        print(input.name)
    #     ort_session_inputs[str(input.name)] = np.random.randint(2, size=(7, 128))
    d = []
    f = []
    for _ in range(7):
        d.append(src)
        f.append(seg)

    ort_session_inputs["input.1"] = np.array(d)
    ort_session_inputs["1"] = np.random.randint(2, size=(7, 128))

    ort_session_inputs["input.3"] = np.array(f)
    return line, ort_session_test(args, ort_session, ner_name, ort_session_inputs, id_label)[0]

    # for _ in tqdm(range(1000)):
    #     ort_session_test(args, ort_session, ner_name, ort_session_inputs, id_label)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    args = parser.parse_args()
    args.vocab_path = "./data/zh_vocab.txt"
    args.vocab_path = "./data/zh_vocab.txt"
    args.medical_ner_label_id = "./data/medical_label2id.json"
    args.seq_length = 128
    args.spm_model_path = None
    args.tokenizer = SpaceTokenizer(args)
    line, label = text_predict(args,
                               line="心脏早搏等多是心肌炎或是心脏神经官能症等造成的症状。是可以治愈的你建议不要熬夜。不要有情绪波动。不要吸烟和饮酒等等。可以口服点如胺碘酮等进行治疗就可以治愈了。")
    print(line, label)
