### 葫芦笔记医疗版本

#####命名实体识别

标签

```python

{'TargetedTreatB': 0, 'BodyPartsB': 1, 'AbnormalType': 2, 'IOPB': 3, 'PathogenB': 4, 'Symptom': 5, 'DEPB': 6, 'Disease': 7, 'ImageTB': 8, 'MedEquipB': 9, 'BodyFunction': 10, 'Surgery': 11, 'MedEquip': 12, 'SignsB': 13, 'DiseaseB': 14, 'SurgeryB': 15, 'SymptomB': 16, 'TargetedTreat': 17, 'EnMedOrder': 18, 'EnMedOrderB': 19, 'DEP': 20, 'LabTB': 21, 'O': 22, 'AbnormalTypeB': 23, 'Drug': 24, 'BodyParts': 25, 'BodyFunctionB': 26, 'IOP': 27, 'BodySubstanceB': 28, 'GeneralTest': 29, 'Pathogen': 30, 'ImageT': 31, 'BodySubstance': 32, 'GeneralTestB': 33, 'Signs': 34, 'LabT': 35, 'DrugB': 36}
```

数据集


模型训练命令
```
python finetune/run_ner_medical.py --train_path data/medical_ner.json --dev_path data/medical_ner.json  --output_model_path models/medical_base_part.bin --label2id_path data/medical_label2id.json --vocab_path models/google_zh_vocab.txt --batch_size 16 --epochs_num 50
```

mini版本训练命令
```
python finetune/run_ner_medical.py --train_path data/medical_ner.json --dev_path data/medical_ner.json  --output_model_path models/medical_base_part.bin --label2id_path data/medical_label2id.json --vocab_path models/google_zh_vocab.txt --batch_size 16 --epochs_num 50 --config_path models/bert/mini_config.json --model_level mini
```


推理demo
```
python predict_text_demo.py
```
压力测试demo
```
python predict_onnx_runtime.py
```